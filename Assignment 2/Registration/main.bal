import ballerinax/kafka;
import ballerina/log;
import ballerinax/mongodb;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "group-reg",
    // Subscribes to the topic `test-kafka-topic`.
    topics: ["registration"],

    pollingInterval: 1
};

public type User record {|
    string first_name;
    string last_name;
    int age;
|};

// Create a subtype of `kafka:AnydataConsumerRecord`.
public type UserConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    User value;
|};

mongodb:ConnectionConfig mongoConfig = {
    options: {url: "mongodb+srv://dbUser:dbUserPassword@cluster0.agmhs.mongodb.net/?retryWrites=true&w=majority"}
};
string database = "order";
mongodb:Client mongoClient = check new (mongoConfig, database);

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    remote function onConsumerRecord(UserConsumerRecord[] records) returns error? {
        // The set of Kafka records received by the service are processed one by one.
        io:println("*******************************");
        check from UserConsumerRecord orderRecord in records
            // where orderRecord.value.isValid
            do {
                string|mongodb:Error? res = check mongoClient->insert({
                    "first_name": orderRecord.value.first_name,
                    "last_name": orderRecord.value.last_name,
                    "age": orderRecord.value.age}
                    , "customers");

                if (res is string) {
                    log:printInfo("Created user");
                    log:printInfo(res);
                } else {
                    log:printInfo("Could not create user");
                    io:println(res);
                }
            };
    }
}
